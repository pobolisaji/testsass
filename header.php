<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
    <header id="header">
        <div class="logo1">
            <img src="<?php echo get_template_directory_uri(); ?> /assets/img/logo_UL.png" alt="logo" />
        </div>
        <div class="logo2">
            <img src="<?php echo get_template_directory_uri(); ?> /assets/img/logo_INSA.png" alt="logo" />
        </div>
        <h1>Trust them</h1>
    </header>

    <?php wp_body_open(); ?>

    <div class="contain">
        <div class="frontcontain">
            <p>Home > Trust them</p>
        </div>
        <div class="textcontain">
            <div class="textcontain1">
                <img src="<?php echo get_template_directory_uri(); ?> /assets/img/Ellipse_2.png" alt="logo" />
            </div>
            <div class="textcontain2">
                <div class="textcontain2_title">
                    <h1>Erica ROMAGUERA</h1>
                </div>
                <div class="textcontain2_subtitle">
                    <p>Main dpt: Biosciences</p><br>
                    <p>Context: Exchange programme</p>
                </div>
                <div class="textcontain2_text">
                    <p>Why did you choose INSA Lyon ? Fields of study, laboratories, school reputation, to live a French experience...</p>
                </div>
            </div>

        </div>



    </div>
    
    