let mix = require('laravel-mix');

mix.sass('assets/sass/style.scss', 'assets/dist').options({
    processCssUrls: false
});