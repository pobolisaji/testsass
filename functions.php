<?php

function integration_scss(){
	wp_enqueue_style('style', get_template_directory_uri(). '/assets/dist/style.css', array(), false, 'all');
}

add_action('wp_enqueue_scripts', 'integration_scss');
?>